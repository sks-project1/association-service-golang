package association

import (
	"goservice/models"
	"math"
)

var (
	minSupport    = 0.2
	minConfidence = 0.5
)

// function to overwrite minSupport and minConfidence
func SetMinSupportAndMinConfidence(support float64, confidence float64) {
	minSupport = support 
	minConfidence = confidence
}

// GetAssociations computes associations based on shopping cart, transactions, minSupport, and minConfidence
func GetAssociations(cart []models.Item, transactions [][]models.Item) []models.Item {
	candidates := make([][]models.Item, 0)
	for _, i := range models.Items {
		if !contains(cart, i) {
			candidates = append(candidates, []models.Item{i})
		}
	}

	XTransactions := getAllCartsFromList(cart, transactions)

	NeccessarySupport := int(math.Round(minSupport * float64(len(transactions))))
	NeccessaryConfidence := int(math.Round(minConfidence * float64(len(XTransactions))))

	mostConfidentItemset := make([]models.Item, 0)

	for {
		setsOfLastLayer := make([][]models.Item, 0)
		newMostConfidentItemset := make([]models.Item, 0)
		newHighestConfidence := 0

		for _, list := range candidates {
			support := 0
			for _, transaction := range XTransactions {
				if containsAll(transaction, list) {
					support++
				}
			}
			if support >= NeccessarySupport && support >= NeccessaryConfidence {
				setsOfLastLayer = append(setsOfLastLayer, list)
				if support > newHighestConfidence {
					newMostConfidentItemset = list
					newHighestConfidence = support
				}
			}
		}

		if len(newMostConfidentItemset) == 0 {
			return mostConfidentItemset
		}

		mostConfidentItemset = newMostConfidentItemset
		candidates = createSuperSets(setsOfLastLayer)
	}
}

func contains(slice []models.Item, item models.Item) bool {
	for _, s := range slice {
		if s == item {
			return true
		}
	}
	return false
}

func createSuperSets(lastLayer [][]models.Item) [][]models.Item {
	candidates := make([]models.Item, 0)
	superSet := make([][]models.Item, 0)

	// Find all items that are in the last layer at least once
	for _, item := range models.Items {
		for _, list := range lastLayer {
			if containsItem(list, item) {
				candidates = append(candidates, item)
				break
			}
		}
	}

	// Remove candidates that cannot create supersets
	for range lastLayer[0] {
		candidates = candidates[1:]
	}

	// For each candidate, create supersets
	for _, candidate := range candidates {
		for _, list := range lastLayer {
			// Early stopping if the first item in a list is the candidate
			if list[0] == candidate {
				break
			}

			foundHigherItem := false
			for _, item := range list {
				// Stop if it contains the candidate or if we found a lexically higher item
				if item == candidate || indexOf(models.Items, item) > indexOf(models.Items, candidate) {
					foundHigherItem = true
					break
				}

				// If we reached the end of the list, we can add the candidate
				if item == list[len(list)-1] {
					newList := append([]models.Item(nil), list...)
					newList = append(newList, candidate)

					// Find out if all subsets of the new list are in the last layer
					allSubsetsAreContained := true
					for j := 0; j < len(newList); j++ {
						subset := append([]models.Item(nil), newList...)
						subset = append(subset[:j], subset[j+1:]...)
						if !containsSublist(lastLayer, subset) {
							allSubsetsAreContained = false
							break
						}
					}

					if allSubsetsAreContained {
						superSet = append(superSet, newList)
					}
				}
			}

			if foundHigherItem {
				break
			}
		}
	}
	return superSet
}

func containsSublist(lastLayer [][]models.Item, subset []models.Item) bool {
	for _, list := range lastLayer {
		if equal(list, subset) {
			return true
		}
	}
	return false
}

func equal(a, b []models.Item) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func indexOf(items []models.Item, item models.Item) int {
	for i, it := range items {
		if it == item {
			return i
		}
	}
	return -1
}

func containsItem(list []models.Item, item models.Item) bool {
	for _, listItem := range list {
		if listItem == item {
			return true
		}
	}
	return false
}

func containsAll(transaction []models.Item, items []models.Item) bool {
	for _, item := range items {
		if !containsItem(transaction, item) {
			return false
		}
	}
	return true
}

// returns the subset of all transactions that contain all items in the given list
func getAllCartsFromList(items []models.Item, transactions [][]models.Item) [][]models.Item {
	result := make([][]models.Item, 0)
	for _, transaction := range transactions {
		if containsAll(transaction, items) {
			result = append(result, transaction)
		}
	}
	return result
}
