package transaction

import (
	"goservice/models"
	"math/rand"
)

type TransactionDB struct {
    Transactions [][]models.Item
}
const (
	seed                 = 123456789
	numberOfTransactions = 1000
)

func NewTransactionDB() *TransactionDB {
	rand.Seed(seed)

	db := &TransactionDB{
		Transactions: make([][]models.Item, 0),
	}

	for i := 0; i < numberOfTransactions; i++ {
		transaction := make([]models.Item, 0)

		if rand.Intn(2) == 1 {
			transaction = append(transaction, models.NewItem("Beer"))
		}
		if rand.Intn(2) == 1 {
			transaction = append(transaction, models.NewItem("Diaper"))
		}
		if rand.Intn(2) == 1 {
			transaction = append(transaction, models.NewItem("Milk"))
		}
		if rand.Intn(2) == 1 {
			transaction = append(transaction, models.NewItem("Bread"))
		}
		if rand.Intn(2) == 1 {
			transaction = append(transaction, models.NewItem("Eggs"))
		}
		if rand.Intn(2) == 1 {
			transaction = append(transaction, models.NewItem("Coke"))
		}

		db.Transactions = append(db.Transactions, transaction)
	}

	return db
}

func AddTransaction(db *TransactionDB, transaction []models.Item) {
    db.Transactions = append(db.Transactions, transaction)
}

func GetTransactions(db *TransactionDB) [][]models.Item {
	return db.Transactions
}