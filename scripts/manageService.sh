#!/bin/bash

# Function to build the Docker image
build_image() {
    echo "Building the Docker image..."
    docker build -t my-go-service ../.
}

# Function to run the Docker container
run_container() {
    echo "Running the Docker container..."
    docker run -d -p 8090:8090 --name my-go-container my-go-service
}

# Function to stop the running Docker container
stop_container() {
    echo "Stopping the Docker container..."
    docker stop my-go-container
    docker rm my-go-container
}

# Check the input parameters
case "$1" in
    build | b)
        build_image
        ;;
    run | r)
        build_image
        run_container
        ;;
    stop | s)
        stop_container
        ;;
    *)
        echo "Usage: $0 [build | b | run | r | stop | s]"
        echo "Options:"
        echo "  build, b   - Build the Docker image"
        echo "  run, r     - Build the image and run the Docker container"
        echo "  stop, s    - Stop the running Docker container"
        ;;
esac