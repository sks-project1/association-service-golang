package association

import (
	"goservice/models"
	"reflect"
	"testing"
)


var testTransactions [][]models.Item
var beerCart []models.Item
var milkCart []models.Item
var cokeCart []models.Item
var beerMilkCart []models.Item
var milkCokeCart []models.Item
var beerCokeCart []models.Item
var beerMilkCokeCart []models.Item

func setup() {
	testTransactions = [][]models.Item{
		{{Name: "Bread"}, {Name: "Milk"}},
		{{Name: "Bread"}, {Name: "Diaper"}, {Name: "Beer"}, {Name: "Eggs"}},
		{{Name: "Milk"}, {Name: "Diaper"}, {Name: "Beer"}, {Name: "Coke"}},
		{{Name: "Bread"}, {Name: "Milk"}, {Name: "Diaper"}, {Name: "Beer"}},
		{{Name: "Bread"}, {Name: "Milk"}, {Name: "Diaper"}, {Name: "Coke"}},
	}

	beerCart = []models.Item{
		{Name: "Beer"},
	}

	milkCart = []models.Item{
		{Name: "Milk"},
	}

	cokeCart = []models.Item{
		{Name: "Coke"},
	}

	beerMilkCart = []models.Item{
		{Name: "Beer"},
		{Name: "Milk"},
	}

	milkCokeCart = []models.Item{
		{Name: "Milk"},
		{Name: "Coke"},
	}

	beerCokeCart = []models.Item{
		{Name: "Beer"},
		{Name: "Coke"},
	}

	beerMilkCokeCart = []models.Item{
		{Name: "Beer"},
		{Name: "Milk"},
		{Name: "Coke"},
	}
}

func TestGetSuperSets(t *testing.T) {
	setup()

	// test the generation of the beer cart
	result := createSuperSets([][]models.Item{beerCart})
	assertEqual(t, 0, len(result))

	// test the generation of the milk cart
	result = createSuperSets([][]models.Item{milkCart})
	assertEqual(t, 0, len(result))

	// test the generation of the coke cart
	result = createSuperSets([][]models.Item{cokeCart})
	assertEqual(t, 0, len(result))

	// test the generation of the beer milk cart
	result = createSuperSets([][]models.Item{beerCart, milkCart})
	assertEqual(t, 1, len(result))
	assertContain(t, result, beerMilkCart)

	result = createSuperSets([][]models.Item{beerCart, milkCart, cokeCart})
	assertEqual(t, 3, len(result))
	assertContain(t, result, beerMilkCart)
	assertContain(t, result, milkCokeCart)
	assertContain(t, result, beerCokeCart)

	result = createSuperSets([][]models.Item{beerMilkCart, milkCokeCart})
	assertEqual(t, 0, len(result))

	result = createSuperSets([][]models.Item{beerMilkCart, milkCokeCart, beerCokeCart})
	assertEqual(t, 1, len(result))
	assertContain(t, result, beerMilkCokeCart)
}

func TestAssociationWithFullCart(t *testing.T) {
	setup()

	cart := []models.Item{
		{Name: "Beer"},
		{Name: "Milk"},
		{Name: "Diaper"},
		{Name: "Coke"},
		{Name: "Eggs"},
		{Name: "Bread"},
	}
	result := GetAssociations(cart, [][]models.Item{})
	assertEqual(t, 0, len(result))
}

func TestAssociationWithoutFrequents(t *testing.T) {
	setup()
	SetMinSupportAndMinConfidence(2.0, 2.0)
	result := GetAssociations(beerCart, testTransactions)
	SetMinSupportAndMinConfidence(0.2, 0.5)
	assertEqual(t, 0, len(result))
}

func TestGetAllCartsFromList(t *testing.T) {
	setup()

	beerCarts := getAllCartsFromList(beerCart, testTransactions)
	assertEqual(t, 3, len(beerCarts))
	for _, cart := range beerCarts {
		assertContainItem(t, cart, models.Item{Name: "Beer"})
		assertContainList(t, testTransactions, cart)
	}

	milkCarts := getAllCartsFromList(milkCart, testTransactions)
	assertEqual(t, 4, len(milkCarts))
	for _, cart := range milkCarts {
		assertContainItem(t, cart, models.Item{Name: "Milk"})
		assertContainList(t, testTransactions, cart)
	}

	beerMilkCarts := getAllCartsFromList(beerMilkCart, testTransactions)
	assertEqual(t, 2, len(beerMilkCarts))
	for _, cart := range beerMilkCarts {
		assertContainItem(t, cart, models.Item{Name: "Beer"})
		assertContainItem(t, cart, models.Item{Name: "Milk"})
		assertContainList(t, testTransactions, cart)
	}
}

func TestFrequentItemSets(t *testing.T) {
	setup()

	SetMinSupportAndMinConfidence(0.3, 0.5)

	result := GetAssociations(beerCart, testTransactions)
	assertEqual(t, 2, len(result))
	assertContainItem(t, result, models.Item{Name: "Diaper"})
	assertContainItem(t, result, models.Item{Name: "Milk"})

	result = GetAssociations(milkCart, testTransactions)
	assertEqual(t, 2, len(result))
	assertContainItem(t, result, models.Item{Name: "Beer"})
	assertContainItem(t, result, models.Item{Name: "Diaper"})

	result = GetAssociations(beerMilkCart, testTransactions)
	assertEqual(t, 1, len(result))
	assertContainItem(t, result, models.Item{Name: "Diaper"})

	SetMinSupportAndMinConfidence(0.2, 0.5)
}

// Helper functions for assertions

func assertEqual(t *testing.T, expected, actual interface{}) {
	t.Helper()
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("Expected: %v, but got: %v", expected, actual)
	}
}

func assertContain(t *testing.T, list [][]models.Item, item []models.Item) {
	t.Helper()
	for _, v := range list {
		if reflect.DeepEqual(v, item) {
			return
		}
	}
	t.Errorf("Expected list to contain: %v", item)
}

func assertContainItem(t *testing.T, list []models.Item, item models.Item) {
	t.Helper()
	for _, v := range list {
		if reflect.DeepEqual(v, item) {
			return
		}
	}
	t.Errorf("Expected list to contain: %v", item)
}

func assertContainList(t *testing.T, lists [][]models.Item, list []models.Item) {
	t.Helper()
	for _, l := range lists {
		if reflect.DeepEqual(l, list) {
			return
		}
	}
	t.Errorf("Expected lists to contain: %v", list)
}
