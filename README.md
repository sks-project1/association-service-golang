# Association Service Golang




# Local Testing

## Docker Operations

To facilitate building and running the associated Go service using Docker, a Bash script is provided in the `scripts` directory.

### Docker Operations Script

The `manageService.sh` script in the `scripts` directory contains commands for building, running, and stopping the Docker container associated with this service.

#### Usage:

- **Build the Docker image:**
  ```bash
  ./scripts/manageService.sh build
  ```
- **Run the Docker container:**
  ```bash
  ./scripts/manageService.sh run
  ```
- **Stop the Docker container:**
  ```bash
  ./scripts/manageService.sh stop
  ```

This script simplifies the Docker-related operations for building, running, and stopping the service's Docker container.

### Test Endpoints

Another Bash script `testEndpoint.sh` is available in the `scripts` directory. This script demonstrates how to interact with the service endpoints using cURL commands. Only when the service is running via docker the endpoints can be tested that way.

#### Usage:

- **Test the endpoints:**
  ```bash
  ./scripts/testEndpoint.sh
  ```

This script performs operations on the service endpoints, including adding items to `TransactionDB`, retrieving the `TransactionDB`, and getting associations.

---

Both scripts are designed to facilitate different aspects of working with the Go service: the `docker_operations.sh` script assists with Docker-related operations, while the `testEndpoint.sh` script demonstrates interactions with the service's endpoints. Adjust the paths and commands as needed to match your specific project structure and requirements.

## Local Testing
The unit tests in association_test.go cover various scenarios and functionalities of the association algorithm. To run the tests, execute:

```bash
go test ./association/ -v
```