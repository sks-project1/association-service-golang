package main

import (
	"log"
	"net/http"
	"goservice/handlers"
	"goservice/transaction"
)

func main() {
	// Initializing TransactionDB
	TransactionDB := transaction.NewTransactionDB()

	// Passing the address of MyTransactionDB to SetTransactionDB
	handlers.SetTransactionDB(TransactionDB)

	// Define your endpoints
	http.HandleFunc("/api/checkout", handlers.AddCart)
	http.HandleFunc("/api/database", handlers.Database)
	http.HandleFunc("/api/associations", handlers.Association)

	// Start the server
	log.Fatal(http.ListenAndServe(":8090", nil))
}
