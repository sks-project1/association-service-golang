# Start from a base image containing the Go runtime
FROM golang:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the local code to the container's working directory
COPY . .

# Build the Go application
RUN go build -o app .

# Command to run the executable
CMD ["./app"]