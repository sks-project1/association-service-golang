package models

type Item struct {
    Name string
}

// List of predefined items
var Items = []Item{
	{Name: "Beer"},
	{Name: "Diaper"},
	{Name: "Milk"},
	{Name: "Bread"},
	{Name: "Eggs"},
	{Name: "Coke"},
}

func NewItem(name string) Item {
	return Item{Name: name}
}

func (i Item) GetName() string {
	return i.Name
}

func (i *Item) SetName(name string) {
	i.Name = name
}

func (i Item) Equals(obj interface{}) bool {
	if item, ok := obj.(Item); ok {
		return item.Name == i.Name
	}
	return false
}