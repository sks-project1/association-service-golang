#!/bin/bash

# Function to test adding items to TransactionDB (Endpoint1)
add_items() {
    echo "Adding items to TransactionDB (AddCart)"
    curl -X POST -H "Content-Type: application/json" -d '[{"Name": "Beer"}, {"Name": "Diaper"}, {"Name": "Milk"}]' http://localhost:8090/api/checkout
    curl -X POST -H "Content-Type: application/json" -d '[{"Name": "Diaper"}, {"Name": "Beer"}, {"Name": "Milk"}]' http://localhost:8090/api/checkout
    curl -X POST -H "Content-Type: application/json" -d '[{"Name": "Beer"}, {"Name": "Milk"}, {"Name": "Diaper"}]' http://localhost:8090/api/checkout
    echo ""
}

# Function to test retrieving TransactionDB (Endpoint2)
retrieve_transactiondb() {
    echo "Retrieving TransactionDB (Endpoint2)"
    curl http://localhost:8080/api/Database
    echo ""
}

# Function to test getting association (Endpoint3)
get_association() {
    echo "Getting association (Endpoint3)"
    curl -X POST -H "Content-Type: application/json" -d '[{"Name": "Bread"}]' http://localhost:8090/api/associations
    echo ""
}

# Test the endpoints
add_items
retrieve_transactiondb
get_association