package handlers

import (
    "encoding/json"
    "net/http"
	"goservice/models"
    "goservice/transaction"
	"goservice/association"
)

// Global variable to store TransactionDB instance
var TransactionDB *transaction.TransactionDB

func SetTransactionDB(db *transaction.TransactionDB) {
	TransactionDB = db
}

func AddCart(w http.ResponseWriter, r *http.Request) {
	// Parse incoming JSON data
    var items []models.Item
    err := json.NewDecoder(r.Body).Decode(&items)
    if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

    // Add items to the TransactionDB
    transaction.AddTransaction(TransactionDB, items)

    // Respond with success message
    w.WriteHeader(http.StatusOK)
    w.Write([]byte("Items added to TransactionDB"))
}

func Database(w http.ResponseWriter, r *http.Request) {
	// Marshal TransactionDB to JSON
    jsonResponse, err := json.Marshal(TransactionDB)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }

    // Respond with TransactionDB data
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusOK)
    w.Write(jsonResponse)
}

func Association(w http.ResponseWriter, r *http.Request) {
	var shoppingCart []models.Item
	err := json.NewDecoder(r.Body).Decode(&shoppingCart)
	println("Shopping cart:")

	// Print shopping cart to string
	for _, item := range shoppingCart {
		println(item.Name)
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Access TransactionDB instance to get transactions
	transactions := transaction.GetTransactions(TransactionDB)

	// Compute associations using the association package
	associations := association.GetAssociations(shoppingCart, transactions)

	// Marshal associations to JSON
	jsonResponse, err := json.Marshal(associations)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with the list of associations
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonResponse)
}